#### Requirment:
This project need to run under java 8

#### Running comment line (for ecplise test and build the jar)
```
./gradlew eclipse
./gradlew test
./gradle build
```
##### if csv in folder outside, you can use absolte path:
```
java -jar build/libs/concourrent-1.0.jar -cfa /the/path/to/your/test.csv
```
###### ex. my case is :
```
java -jar build/libs/concourrent-1.0.jar -cfa /home/tongz/project/java/gwava/concourrent/src/main/resources/test.csv
```
Notice: The file can be in whatever the dir, no re-complie is required


##### if csv in project resource folder, you can use short path:
* File need to be local at ``` src/main/resources ```

```
java -jar build/libs/concourrent-1.0.jar -cfr test.csv
```
Notice: Each time if your csv file is changed you have to recomplie the project

