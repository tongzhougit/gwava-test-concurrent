package parser.exception;

/** 
 * No file path found exception 
 * 
 * @author 
 * @version 
 */
public class NoFilePathFoundException extends BaseException
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** 
     * Constructor
     * 
     * @param msg 
     */
    public NoFilePathFoundException(String msg)
    {
        super(msg);
    }
}
