package parser.exception;

/** 
 * Base excpetion 
 * 
 * @author Tong Zhou
 * @version 
 */
public class BaseException extends Exception
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** 
     * Constructor
     * 
     * @param msg 
     */
    public BaseException(String msg)
    {
        super(msg);
    }
}
