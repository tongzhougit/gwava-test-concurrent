package parser.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import parser.thread.LineProcessThread;
import parser.exception.BaseException;
import parser.exception.NoFilePathFoundException;

/** 
 * File process service in concurrent 
 * 
 * @author Tong Zhou
 * @version 
 */
public class FileProcessService implements ServiceInterface
{

    @Parameter(names = { "--csvFileAbsolutePath", "-cfa" }, required = false, description = "The full path of csv file")
    private String csvFileAbsolutePath;

    @Parameter(names = { "--csvFileFromResource", "-cfr" }, required = false, description = "The file of csv file in resource folder")
    private String csvFileFromResource;

    private ExecutorService executor;

    protected String[] inputs;

    /**
     * Constructor
     */
    public FileProcessService(String[] inputs) 
    {
        this.inputs   = inputs;
        int nThreads  = Runtime.getRuntime().availableProcessors();
        this.executor = Executors.newFixedThreadPool(nThreads);
    }

    
    /** 
     *  Run file process 
     * 
     * @throws BaseException 
     */
    @Override
    public void run() throws BaseException  {
        try (InputStream source = getInputStreamByInputs(this.inputs)) {

            AtomicInteger index = new AtomicInteger(); 

            try (BufferedReader reader = readFileByBufferReader(source)) {
                reader.lines()
                    .skip(1)
                    .distinct()
                    .forEach(line -> processSingleLine(line, index));
                this.executor.shutdown();           

            } catch (IOException e) {
                throw new IOException(e);
            }
        
        } catch (NoFilePathFoundException | ParameterException | IOException  e) {
            throw new BaseException(e.getMessage());
        }
    }

    /** 
     * Read the file  
     * 
     * @param source 
     * @return 
     */
    private BufferedReader readFileByBufferReader(InputStream source)
    {
        return new BufferedReader(new InputStreamReader(source)); 
    }

    /** 
     * Single line process
     * 
     * @param line 
     * @param index 
     */
    private void processSingleLine(String line, AtomicInteger index)
    {
        int indexNumber   = index.incrementAndGet();
        String threadName = "Line Thread " + indexNumber;
        LineProcessThread lineProcessThread = new LineProcessThread(threadName, indexNumber, line);
        this.executor.submit(lineProcessThread);
    }


    /** 
     * get input stream 
     * 
     * @param inputs 
     * @return 
     * @throws NoFilePathFoundException 
     * @throws FileNotFoundException 
     * @throws ParameterException 
     */
    private InputStream getInputStreamByInputs(String[] inputs) throws NoFilePathFoundException, FileNotFoundException, ParameterException
    {
        new JCommander(this, inputs); 

        if (csvFileFromResource != null && !csvFileFromResource.isEmpty()) {

            InputStream inputstream = ClassLoader.getSystemClassLoader().getResourceAsStream(csvFileFromResource);

            if (inputstream == null) {
                throw new FileNotFoundException(csvFileFromResource + " (No such file or directory)");
            }

            return inputstream;

        } else if (csvFileAbsolutePath != null && !csvFileAbsolutePath.isEmpty()) {
            return new FileInputStream(csvFileAbsolutePath);

        } else {
            throw new NoFilePathFoundException("Neither resource path nor absolute path found in cmd, please try again");
        }
    }

}
