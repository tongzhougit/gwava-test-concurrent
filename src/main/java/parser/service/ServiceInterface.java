package parser.service;

import parser.exception.BaseException;

/**
 *
 * Service Interface
 *
 * @author Tong Zhou
 */
public interface ServiceInterface
{
    /**
     * Run the Service
     *
     * @throws BaseException
     *
     */
    public void run() throws BaseException;
}
