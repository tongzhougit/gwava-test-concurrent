package parser.thread;

import java.util.concurrent.Callable;

/**
 * Field process thread
 *
 * @author Tong Zhou
 */
public class FieldProcessThread implements Callable<String> 
{
    private final static int TIME_TO_PROCESS_FIELD = 3000;

    private String threadName; 

    private String fieldValue;


    /**
     * {@inheritDoc}
     *
     * @see Object#FieldProcessThread()
     */
    public FieldProcessThread(String threadName, String fieldValue)
    {
       this.threadName = threadName; 
       this.fieldValue = fieldValue;
    }

    /**
     * {@inheritDoc}
     *
     * @Exception
     * @return String 
     */
    @Override
    public String call() throws Exception 
    {
        processField(); 
        return "Feild " + fieldValue + " processed";
    }

    /**
     * Process field 
     *
     * @Exception InterruptedException
     */
    private void processField() throws InterruptedException 
    {
        System.out.println("Thread name: " + this.threadName + " | Process field value: " + this.fieldValue);
        Thread.sleep(TIME_TO_PROCESS_FIELD);  
    }
}
