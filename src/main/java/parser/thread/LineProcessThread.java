package parser.thread;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Process line thread
 *
 * @author Tong Zhou
 */
public class LineProcessThread implements Callable<String> 
{
    
    /** 
     *  
     */
    private final static String CSV_SEPARATOR = ",";

    private String threadName; 

    private int lineNumber;

    private String lineValue;

    private ExecutorService executor;

    /**
     * {@inheritDoc}
     *
     * @see Object#LineProcessThread()
     */
    public LineProcessThread(String threadName, int lineNumber, String lineValue)
    {
       this.threadName = threadName; 
       this.lineNumber = lineNumber;
       this.lineValue  = lineValue;
       int nThreads    = Runtime.getRuntime().availableProcessors();
       this.executor   = Executors.newFixedThreadPool(nThreads);
    }

    /**
     * {@inheritDoc}
     *
     * @Exception
     * @return String 
     */
    @Override
    public String call() throws Exception 
    {
        processLine(); 
        return "Line " + this.lineNumber + " processed";
    }

    /**
     * Process line
     *
     * @Exception InterruptedException
     */
    private void processLine() throws InterruptedException 
    {
        System.out.println("Thread name: " + this.threadName + " | process line number: " + this.lineNumber);
        AtomicInteger index = new AtomicInteger(); 

        Arrays.stream(this.lineValue.split(CSV_SEPARATOR))
            .forEach(field -> processSingleFields(field, index));

        this.executor.shutdown();
    }

    private void processSingleFields(String field, AtomicInteger index)
    {
        int indexNumber = index.incrementAndGet();
        FieldProcessThread fieldProcessThread = new FieldProcessThread("Field Thread " + indexNumber, field);
        this.executor.submit(fieldProcessThread);
    }
}
