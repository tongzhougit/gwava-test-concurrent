package parser;

import parser.exception.BaseException;
import parser.service.FileProcessService;

public class Application 
{

    /**
     * Main to start application
     * @param <T>
     *
     * @param args
     */
    public static <T> void main(String[] args)
    {
        try {
            FileProcessService fieldProcessService = new FileProcessService(args);
            fieldProcessService.run();
        } catch (BaseException e) {
            System.err.println(e.getMessage());
        }
    }
}
