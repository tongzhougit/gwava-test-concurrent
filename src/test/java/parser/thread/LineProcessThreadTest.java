package parser.thread;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import parser.thread.LineProcessThread;

public class LineProcessThreadTest
{
    private List<String> sampleList;

    @Before
    public void setUp() 
    {
        this.sampleList = Arrays.asList("Field1,Field2", "Filed1,Field2,Field3");
    }

    @Test
    public <T> void lineProcessThread()
    {
        int nThreads             = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        AtomicInteger index      = new AtomicInteger(); 
        List<Future<T>> list     = new ArrayList<Future<T>>();

        this.sampleList.stream()
            .forEach(
                line -> {
                    int indexNumber = index.incrementAndGet();
                    LineProcessThread lineProcessThread = new LineProcessThread(
                        "Line Thread " + indexNumber,
                        indexNumber,
                        line
                    );
                    @SuppressWarnings("unchecked")
                    Future<T> submit = (Future<T>) executor.submit(lineProcessThread);
                    list.add(submit);
                }
            );

        executor.shutdown();
        int counter = 1;
        for (Future<T> future : list) {
            try {
                assertEquals("Line " + counter + " processed", future.get());
            } catch (Exception e) {
                fail(e.getMessage());
            }
            counter++;
        }
    }
}
