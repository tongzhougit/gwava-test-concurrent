package parser.thread;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.Future;

import org.junit.Before;
import org.junit.Test;

import parser.thread.FieldProcessThread;

public class FieldProcessThreadTest
{
    private List<String> sampleList;

    @Before
    public void setUp() 
    {
        this.sampleList = Arrays.asList("Field1", "Field2", "Field3", "Field4");
    }

    @Test
    public <T> void fieldProcessThread()
    {
        int nThreads             = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        AtomicInteger index      = new AtomicInteger(); 
        List<Future<T>> list     = new ArrayList<Future<T>>();

        this.sampleList.stream()
            .forEach(
                field -> {
                    int indexNumber = index.incrementAndGet();
                    FieldProcessThread fieldProcessThread = new FieldProcessThread(
                        "Field Thread " + indexNumber, 
                        field
                    );
                    @SuppressWarnings("unchecked")
                    Future<T> submit = (Future<T>) executor.submit(fieldProcessThread);
                    list.add(submit);
                }
            );

        executor.shutdown();
        int counter = 0;
        for (Future<T> future : list) {
            try {
                assertEquals("Feild " + this.sampleList.get(counter) + " processed", future.get());
            } catch (Exception e) {
                fail(e.getMessage());
            }
            counter++;
        }
    }
}
