package parser.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import parser.exception.BaseException;
import parser.exception.NoFilePathFoundException;
import parser.service.FileProcessService;

/** 
 *  Test file process service
 */
public class FileProcessServiceTest
{
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private FileProcessService fileProcessServiceFileInResourceSuccess;
    private FileProcessService fileProcessServiceFileInResource;
    private FileProcessService fileProcessServiceFileAbsolet;
    private FileProcessService fileProcessServiceWithoutFilePath;
    private FileProcessService fileProcessServiceWithWrongParam;

    /** 
     * Setup the sample data 
     */
    @Before
    public void setUp() 
    {
        String[] goodCsvPath       = {"-cfr", "test.csv"};
        String[] badCsvPath        = {"-cfr", "badcsv"};
        String[] badCsvAbsoltePath = {"-cfa", "badcsv"};
        String[] empty             = {};
        String[] wrong             = {"-df", "sdf"};
        fileProcessServiceFileInResourceSuccess  = new FileProcessService(goodCsvPath);
        fileProcessServiceFileInResource         = new FileProcessService(badCsvPath);
        fileProcessServiceFileAbsolet            = new FileProcessService(badCsvAbsoltePath);
        fileProcessServiceWithoutFilePath        = new FileProcessService(empty);
        fileProcessServiceWithWrongParam         = new FileProcessService(wrong);
        System.setErr(new PrintStream(errContent));
        System.setOut(new PrintStream(outContent));
    }

    /** 
     *  clean up the stream
     */
    @After
    public void cleanUpStreams() 
    {
        System.setErr(null);
    }

    @Test
    public <T> void runSuccess()
    {
        try {
            fileProcessServiceFileInResourceSuccess.run();
        } catch (BaseException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public <T> void runFileInResource()
    {
        try {
            fileProcessServiceFileInResource.run();
        } catch (BaseException e) {
            assertEquals("badcsv (No such file or directory)", e.getMessage());
        }
    }

    @Test
    public <T> void runFileAbsolet()
    {
        try {
        fileProcessServiceFileAbsolet.run();
        } catch (BaseException e) {
            assertEquals("badcsv (No such file or directory)", e.getMessage());
        }
    }


    @Test
    public <T> void runFileWithoutPath()
    {
        try {
            fileProcessServiceWithoutFilePath.run();
        } catch (BaseException e) {
            assertEquals("Neither resource path nor absolute path found in cmd, please try again", e.getMessage());
        }
    }

    @Test
    public <T> void runFileWithWrongParam()
    {
        try {
            fileProcessServiceWithWrongParam.run();
        } catch (BaseException e) {
            assertEquals("Unknown option: -df", e.getMessage());
        }
    }

}
