A) Please provide a java project using Eclipse demonstrating your knowledge/skills with concurrency and multi-threading
1) This application will be given a path to a csv, comma-separated values, file that it must process.
2) This file will be several 100s of GBs of data and will contain multiple entries with 10 fields each.
3) The 10 fields that repeat over and over will be considered a record that needs to be processed.  We will refer to these fields as Field1, Field2, ..... to Field10.
4) For the sake of this exercise assume that the processing of each records will take between 3 and 10 seconds.  Please simulate this with a sleep.
5) Please create a multi-threaded application that will process this file as quickly as possible.  Assume a dedicated 8 core machine with 16 GB of ram.
6) Please comment the code explaining why you've written the code the way you have and feel free to discuss other ways you considered writing the code.  This is your chance to illustrate your understanding of mutli-threaded and concurrancy programming.
